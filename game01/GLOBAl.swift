//
//  CONSTANTS.swift
//  game01
//
//  Created by Дмитрий Агапов on 26.08.17.
//  Copyright © 2017 Игорь Артемьев. All rights reserved.
//

import Foundation
import UIKit


// SOME GLOBAL VAR
var SCREEN_WIDTH : CGFloat = 0
var SCREEN_HEIGHT : CGFloat = 0
var COUNT_POINT = 0
var LIVE_POINT = 1
var GAME_FINISH = 0





//All CONSTANTS
let SPEED = CGVector(dx: -1, dy: -1)
let PLATFORMA_SPEED = CGVector(dx: 1, dy: 0)
let BLOCK_SIZE = CGSize(width: 30, height: 30)
let PLATFORMA_SIZE = CGSize(width: 80, height: 30)
let BALL_SIZE = CGSize(width: 30, height: 30)

