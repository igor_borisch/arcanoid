//
//  ViewController.swift
//  game01
//
//  Created by Игорь Артемьев on 07.08.17.
//  Copyright © 2017 Игорь Артемьев. All rights reserved.
//

import UIKit
import SpriteKit


class ViewController: UIViewController, UITextFieldDelegate {
    var test: blocks!
    var gameBall: ball!
    var gamePlatform: platform!
    var gameTimer: Timer! //dobavil dlya sozdaniya gameovera
    let screenSize: CGRect = UIScreen.main.bounds
    var backgroundMusic: SKAudioNode!
    
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var button1: UIButton!
    
    override func viewDidLoad() {
        SCREEN_WIDTH = screenSize.width
        SCREEN_HEIGHT = screenSize.height
        playMainSound(name:"music", ext:"mp3")  //вызов музыки
        self.test = blocks(veiw: self.view)
        self.gameBall = ball(veiw: self.view)
        self.gamePlatform = platform(veiw: self.view)
        super.viewDidLoad()
        label1.text = String(0)
        gameTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ViewController.allMove), userInfo: nil, repeats: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func allMove(){
        gameBall.interaction(object: gamePlatform)
        gameBall.speed = test.interaction(object: gameBall)
        gameBall.move()
        gamePlatform.move()
        label1.text = String(COUNT_POINT)
        if (LIVE_POINT == 0) {
            gameTimer.invalidate()
            //print (gameTimer)
            gameTimer = nil
            label1.text = "GAMEOVER"
            button1.isHidden = false
            button1.backgroundColor = UIColor.red
            test.del()
            self.gameBall.ballObject.center.x = self.gamePlatform.ballObject.center.x
            self.gameBall.ballObject.center.y = self.gamePlatform.ballObject.center.y-CGFloat(BALL_SIZE.width)
            mainSound?.stop()
            Punch?.stop()
            gameOver(name:"gameover", ext:"mp3")  //вызов музыки
            
        }
        if (GAME_FINISH == 1) {
            gameTimer.invalidate()
            //print (gameTimer)
            gameTimer = nil
            label1.text = "Victory"
            button1.isHidden = false
            button1.backgroundColor = UIColor.blue
            self.gameBall.ballObject.center.x = self.gamePlatform.ballObject.center.x
            self.gameBall.ballObject.center.y = self.gamePlatform.ballObject.center.y-CGFloat(BALL_SIZE.width)
        }
    }

    @IBAction func leftOutside(_ sender: Any) {
        self.gamePlatform.speed.dx = 0
    }
    @IBAction func leftMove(_ sender: Any) {
        self.gamePlatform.speed.dx = -2*PLATFORMA_SPEED.dx
    }
    
    @IBAction func rightOutside(_ sender: Any) {
        self.gamePlatform.speed.dx = 0
    }
    @IBAction func rightMove(_ sender: Any) {
        self.gamePlatform.speed.dx = 2*PLATFORMA_SPEED.dx
    }
    
    @IBAction func restart(_ sender: Any) { //zanovo vzvoditsia timer
        gameTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ViewController.allMove), userInfo: nil, repeats: true)
        COUNT_POINT = 0
        LIVE_POINT = 1
        GAME_FINISH = 0
        button1.isHidden = true
        self.test = blocks(veiw: self.view)
        playMainSound(name:"music", ext:"mp3")  //вызов музыки
        Over?.stop()
    }
}

