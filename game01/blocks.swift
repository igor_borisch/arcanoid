//
//  block1.swift
//  16
//
//  Created by Игорь Артемьев on 07.08.17.
//  Copyright © 2017 Игорь Артемьев. All rights reserved.
//

import Foundation
import UIKit

class blocks {
    
    
    var imgMassiv: Array<UIImageView>! = []
    
    init(veiw: UIView) {
        let coordMass = createCoordMass(numY: 3)
        for i in coordMass{
            let someImg = UIImageView(image: UIImage(named: "block.png")!)
            someImg.frame = CGRect(origin: i, size: BLOCK_SIZE)
            veiw.addSubview(someImg)
            imgMassiv.append(someImg)
        }
    }
    
    func remove(number: Int) {
        self.imgMassiv[number].removeFromSuperview()
        self.imgMassiv.remove(at:number)
        COUNT_POINT = COUNT_POINT + 50
        if self.imgMassiv.count == 0 {
            GAME_FINISH = 1
        }
    }
    
    func interaction(object: ball) -> CGVector {
        var count = 0
        for block in self.imgMassiv{
            if pow(Float(object.ballObject.center.y) - Float(block.center.y),2.0) <= pow(Float(BLOCK_SIZE.height/2 + BALL_SIZE.height/2),2.0) && pow(Float(object.ballObject.center.x) - Float(block.center.x),2.0) <= pow(Float(BLOCK_SIZE.width/2 + BALL_SIZE.width/2),2.0) {
                self.remove(number: count)
                playPunch(name: "udar", ext: "mp3")
                if pow(Float(object.ballObject.center.x) - Float(block.center.x),2.0) == pow(Float(BLOCK_SIZE.width/2 + BALL_SIZE.width/2),2.0) {
                    return CGVector(dx : -object.speed.dx, dy: object.speed.dy)
                }
                return CGVector(dx : object.speed.dx, dy: -object.speed.dy)
            }
            count = count + 1
        }
        return object.speed
        
    }
    func del() {
        for _ in self.imgMassiv {
            self.imgMassiv[0].removeFromSuperview()
            self.imgMassiv.remove(at:0)
        }
    }
//pow(Float(BLOCK_SIZE.height/2 - BALL_SIZE.height/2),2.0)
}
