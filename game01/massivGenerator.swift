//
//  massivGenerator.swift
//  game01
//
//  Created by Игорь Артемьев on 08.08.17.
//  Copyright © 2017 Игорь Артемьев. All rights reserved.
//

import Foundation
import UIKit



func createCoordMass(numY: Int) ->Array<CGPoint> {
    let size = Int(BLOCK_SIZE.width)
    let numberOfWidth = (Int(SCREEN_WIDTH)-size)/size
    let numberOfHeight = numY
    
    var massCoord: Array<CGPoint>! = []
    var yCoord = 0
    for _ in 0...numberOfHeight-1{
        var xCoord = ((Int(SCREEN_WIDTH) - numberOfWidth*Int(size))/2)
        
        for _ in 0...numberOfWidth-1{
            
            massCoord.append(CGPoint(x: xCoord, y: yCoord))
            
            xCoord = xCoord + size
        }
        yCoord = yCoord + size
    }
    return (massCoord)
}
