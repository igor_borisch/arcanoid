//
//  music.swift
//  game01
//
//  Created by Игорь Артемьев on 01.09.17.
//  Copyright © 2017 Игорь Артемьев. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation

func playSound() {
    let url = Bundle.main.url(forResource: "ClickSound", withExtension: "mp3")!
    
    do {
        player = try AVAudioPlayer(contentsOf: url)
        guard let player = player else { return }
        
        player.prepareToPlay()
        player.play()
    } catch let error as NSError {
        print(error.description)
    }
}
