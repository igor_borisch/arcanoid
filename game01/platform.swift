//
//  platform.swift
//  game01
//
//  Created by Дмитрий Агапов on 26.08.17.
//  Copyright © 2017 Игорь Артемьев. All rights reserved.
//

import Foundation
import UIKit


class platform {
    
    
    
    var ballObject  :  UIImageView!
    var speed = CGVector(dx: 0, dy: 0)
    
    init(veiw: UIView) {
        
        let ballPosition = CGPoint(x: SCREEN_WIDTH/2, y: 0.75*SCREEN_HEIGHT) 
        let someImg = UIImageView(image: UIImage(named: "plaforma.jpg")!)
        
        someImg.frame = CGRect(origin: ballPosition, size: PLATFORMA_SIZE)
        ballObject = someImg
        veiw.addSubview(someImg)
        
    }
    
    func move(){
        if (self.ballObject.center.x <= PLATFORMA_SIZE.width/2 && self.speed.dx == -2*PLATFORMA_SPEED.dx) || (self.ballObject.center.x >= SCREEN_WIDTH-PLATFORMA_SIZE.width/2 && self.speed.dx == 2*PLATFORMA_SPEED.dx) {
            self.speed.dx = 0
        }
        self.ballObject.center.x = self.ballObject.center.x + self.speed.dx
    }
    
    
}
