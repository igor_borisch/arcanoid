//
//  sound.swift
//  game01
//
//  Created by Игорь Артемьев on 01.09.17.
//  Copyright © 2017 Игорь Артемьев. All rights reserved.
//

import Foundation
import AVFoundation

var mainSound: AVAudioPlayer?
var Punch: AVAudioPlayer?
var Over: AVAudioPlayer?

func playMainSound(name: String, ext: String) {
    
    guard let url = Bundle.main.url(forResource: name, withExtension: ext) else { return }
    
    do {
        
        mainSound = try AVAudioPlayer(contentsOf: url)
        guard let mainSound = mainSound else { return }
        
        mainSound.play()
    } catch  {
        print("error otsuda")
    }

}

func playPunch(name: String, ext: String) {
    
    guard let url = Bundle.main.url(forResource: name, withExtension: ext) else { return }
    
    do {
        
        Punch = try AVAudioPlayer(contentsOf: url)
        guard let Punch = Punch else { return }
        
        Punch.play()
    } catch  {
        print("error otsuda")
    }

}

func gameOver(name: String, ext: String) {
    
    guard let url = Bundle.main.url(forResource: name, withExtension: ext) else { return }
    
    do {
        
        Over = try AVAudioPlayer(contentsOf: url)
        guard let Over = Over else { return }
        
        Over.play()
    } catch {
        print("error otsuda")
    }
    
}
